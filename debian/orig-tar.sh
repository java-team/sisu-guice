#!/bin/sh -e

VERSION=$2
TAR=../sisu-guice_$VERSION.orig.tar.xz
DIR=sisu-guice-sisu-guice-$VERSION

tar zx --file=$3
rm -f $3
XZ_OPT=--best tar cJf $TAR --exclude '*.jar' --exclude '*.class' --exclude 'javadoc' --exclude 'latest-javadoc' --exclude 'latest-api-diffs' $DIR
rm -rf $DIR
